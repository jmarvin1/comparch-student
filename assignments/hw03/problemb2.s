#problemb2.s
	.set noreorder
    	.data
	
    	.text
    	.globl main
    	.ent main
#int main()
#{
# int a = 2;
# int f = polynomial(a, 3, 4, 5, 6);
# print(a);
# print(f);
# }
main: 
	addi	$s0, $0, 2 #int a=2 
	addi	$s1, $sp, -52
	sw	$ra, 48($s1)
	sw	$s0, 44($s1)
	addi	$t0, $0, 3
	addi 	$t1, $0, 4
	addi	$t2, $0, 5
	addi	$t3, $0, 6
	sw	$t0, 40($s1) #3
	sw	$t1, 36($s1) #4
	sw	$t2, 32($s1) #5
	sw	$t3, 28($s1) #6
	j polynomial
	nop
	lw $ra, 48($s1)
	lw $s0, 44($s1)
	addi $sp, $sp, 52        
	jr $ra    # EXIT
        nop

.end main
#void print(int a)
#{
# // should be implemented with syscall 20
#}
.ent print
print:
	#print x
	lw $t0, 24($s1)
	add $a0, $0, $t0
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall

	#print y
	lw $t0, 20($s1)
	add $a0, $0, $t0
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall

	#print z
	lw $t0, 16($s1)
	add $a0, $0, $t0
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall

	
	#print a
	lw $t0, 44($s1)
	add $a0, $0, $t0
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall
	
	#print f (print z)
	lw $t0, 16($s1)
	add $a0, $0, $t0
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall

	j $ra
	nop
	
.end print
#int sum3(int a, int b, int c) x,y, e
#{
# return a+b+c;
#}
.ent sum3
sum3:
	lw $t3, 20($s1)
	lw $t4, 24($s1)	
	add $t1, $t3, $t4
	lw $t5, 28($s1)
	add $t2, $t1, $t5		
	sw $t2, 16($s1)
	j $ra
	nop
.end sum3
#int polynomial(int a, int b, int c, int d, int e)
#{
# int x;
# int y;
# int z;
# x = a << b;
# y = c << d;
# z = sum3(x, y, e);
# print(x);
# print(y);
# print(z);
# return z;
#}
.ent polynomial
polynomial:
	lw $t0, 40($s1)
	sll $t4, $s0, $t0
	lw $t1, 36($s1)
	lw $t2, 32($s1)
	sll $t5, $t1, $t2
	sw $t4, 24($s1)
	sw $t5, 20($s1)
	j sum3
	nop
	j print
	nop
	j $ra
	nop
.end polynomial
