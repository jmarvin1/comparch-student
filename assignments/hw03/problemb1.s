#problemb1.s
	.set noreorder
    	.data
	
    	.text
    	.globl main
    	.ent main
#int main()
#{
main: 
	# int i; i=s0
	# i =2 
	addi 	$s0, $0, 2 
	addi 	$t1, $0, 0
	addi 	$t2, $0, 1
	addi 	$t4, $0, -4
	add     $s2, $sp, -40      # int A[8];
        sw      $ra, 36($sp)        # *(sp +32) = ra
	sw	$s0, 32($sp)
	add	$s2, $s2, $t4
	sw	$t1, ($s2) #A[0]=0
	add	$s2, $s2, $t4
	sw	$t2, ($s2) #A[1]=1
				
loop_cond:
	slti $t0, $s0, 8
	beq $t0, $0, end #for (int i=2; i<8; i++)
	nop
	add $s2, $s2, $t4
	sub $t5, $s2, $t4
	sub $t6, $t5, $t4
	lw $t8, ($t5)
	lw $t9, ($t6)
	add $t7, $t8, $t9
	sw $t7, ($s2)	#A[i] = A[i-1] + A[i-2];
	add $a0, $0, $t7
	jal print # print(A[i]);
	nop
	addi $s0, $s0, 1
	j loop_cond
	nop

end:
	lw $ra, 36($sp)
	lw $s0, 32($sp)
	addi $sp, $sp, 40        
	jr $ra    # EXIT
        nop
.end main

#void print(int a)
#{
# // should be implemented with syscall 20
#}
.ent print
print:
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall
	jr $ra
	nop
.end print
