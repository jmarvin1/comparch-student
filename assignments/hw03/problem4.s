#problem4.s
	.set noreorder
    	.data
	#typedef struct record {
	# int field0;
	# int field1;
	#} record;
	record: .space 8
    	.text
    	.globl main
    	.ent main
#int main()
#{
main:
# record *r = (record *) MALLOC(sizeof(record));
	li $a0, 8
	ori $v0, $0, 9
	syscall
# r->field0 = 100;
	addi $s0, $0, 100
	sw $s0, ($v0)

# r->field1 = -1;
	addi $s1, $0, -1
	sw $s1, 4($v0)	

end:
	# EXIT;
	ori $v0, $0, 10     
        syscall
    .end main







