#problem3.s
	.set noreorder
    	.data
	A: .space 32
    	.text
    	.globl main
    	.ent main
main: # int main()
	addi $s0, $0, 2 #int i =0
	addi $t1, $0, 0
	addi $t2, $0, 1
	addi $t4, $0, 4
	la $t3, A
	sw $t1, ($t3)	#A[0] = 0;
	add $t3, $t3, $t4
	sw $t2, ($t3)	#A[1]= 1;			
loop_cond:
	slti $t0, $s0, 8
	beq $t0, $0, end #for (int i=2; i<8; i++)
	nop
	add $t3, $t3, $t4
	sub $t5, $t3, $t4
	sub $t6, $t5, $t4
	lw $t8, ($t5)
	lw $t9, ($t6)
	add $t7, $t8, $t9
	sw $t7, ($t3)	#A[i] = A[i-1] + A[i-2];
	add $a0, $0, $t7
	ori $v0, $0, 20 #PRINT_HEX_DEC(A[i]);
	syscall
	addi $s0, $s0, 1
	j loop_cond
	nop

end:
        ori $v0, $0, 10     # exit
        syscall
    .end main


