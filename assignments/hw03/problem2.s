#problem2.s
	.set noreorder
    	.data
list: .byte 1, 25, 7, 9, -1 #char A[] = { 1, 25, 7, 9, -1 };
    	.text
    	.globl main
    	.ent main
main: #int main()
	addi $s0, $0, 0 #i=0
	addi $s1, $0, 0 #max=0
	la $t1, list
	lb $t2, ($t1) # current = A[0]
loop_cond:
	slt $t0, $0, $t2 	#while (current >0)
	beq $t0, $0, end
	slt $t0, $s1, $t2 	#if (current > max)
	beq $t0, $0, fail_if
	nop
	add $s1, $0, $t2 	#max= current
	
fail_if:
	addi $s0, $s0, 1 	#i=i+1
	add $t1, $t1, $s0
	lb $t2, ($t1) 	#current = A[i]    
	j loop_cond    

end:
	add $a0, $0, $s1	
	ori $v0, $0, 20		#print max
	syscall	
	ori $v0, $0, 10     	# exit
        syscall
    .end main

