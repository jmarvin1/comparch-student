#problem5.s
	.set noreorder
    	.data
	#typedef struct elt {
	# int value;
	# struct elt *next;
	#} elt;
	elt: .space 8
    	.text
    	.globl main
    	.ent main
#int main()
#{
main:
# elt *head;
	#s0
# elt *newelt;
	#s1
# newelt = (elt *) MALLOC(sizeof (elt));
	li $a0, 8
	ori $v0, $0, 9
	syscall
	add $s1, $0, $v0
# newelt->value = 1;
	addi $t0, $0, 1
	sw $t0, ($s1)	
# newelt->next = 0;
	addi $t0, $0, 0
	sw $t0, 4($s1)
# head = newelt;
	add $s0, $0, $s1
# newelt = (elt *) MALLOC(sizeof (elt));
	li $a0, 8
	ori $v0, $0, 9
	syscall
	add $s1, $0, $v0
# newelt->value = 2;
	addi $t0, $0, 2
	sw $t0, ($s1)
# newelt->next = head;
	sw $s0, 4($s1)
# head = newelt;
	add $s0, $0, $s1
# PRINT_HEX_DEC(head->value);
	lw $a0, ($s0)
	ori $v0, $0, 20 
	syscall
# PRINT_HEX_DEC(head->next->value);
	lw $t0, 4($s0)
	lw $a0, ($t0)
	ori $v0, $0, 20 
	syscall



end:
# EXIT;
	ori $v0, $0, 10     
        syscall
    .end main
